package EstructurasControl;

import java.util.Scanner;

/***
 * Ejercicios Estructuras de control 6.3 (p�g. 55)
 * Ejercicio 1
 * Vamos a solicitar un n�mero entrero y mostrarlo en pantalla si es par o impar
 * usaremos el operador % para comprobar si es par o impar
 * @author Ferm�n
 *
 */

public class OddOrEven {

	public static void main(String[] args) {
		Scanner readFromConsole = new Scanner(System.in);
		int num;
		String Line = "";
		System.out.println("Introduce un n�mero entero: ");
		Line = readFromConsole.nextLine();
		num = Integer.parseInt(Line);
		if (num%2==1) {
			System.out.println("El n�mero que escribiste es impar");
		} else {
			System.out.println("El n�mero que escribiste es par");
		}	

	}

}
