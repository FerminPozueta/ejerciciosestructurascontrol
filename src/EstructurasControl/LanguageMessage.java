package EstructurasControl;

import java.util.Scanner;

/***
 * Ejercicios Estructuras de control 6.3 (p�g. 55)
 * Ejercicio 7
 * Solicitaremos el nombre de un lenguaje de programaci�n y mostraremos un
 * mensaje distinto en base a cu�l haya sido introducido
 * Mostraremos si en base a tu IMC c�mo est�s
 * @author Ferm�n
 *
 */

public class LanguageMessage {

	public static void main(String[] args) {
		Scanner readfromconsole = new Scanner(System.in);
		String language = "";
		System.out.println("Dinos qu� lenguage te gusta");
		language = readfromconsole.nextLine();
		switch (language) {
		case "C++":
			System.out.println("The best language ever");
			break;
		case "Java":
			System.out.println("The second best language ever");
			break;
		case "JavaScript":
			System.out.println("The language of the present");
			break;
		default:
			System.out.println("Nothing to say about that");
		}

	}

}
