package EstructurasControl;

import java.util.Scanner;

/***
 * Ejercicios Estructuras de control 6.3 (p�g. 55)
 * Ejercicio 3
 * Solicitaremos un n�mero entero
 * Mostraremos si ese n�mero es par y positivo
 * o si es negativo, impar o ambos
 * @author Ferm�n
 *
 */

public class PositiveEven {

	public static void main(String[] args) {
		Scanner readFromConsole = new Scanner(System.in);
		int num;
		String Line = "";
		System.out.println("Introduzca un n�mero");
		Line = readFromConsole.nextLine();
		num = Integer.parseInt(Line);
		if (num%2==0 && num>0) {
			System.out.println("El n�mero "+num+" es par y positivo al mismo tiempo"); 
		} else if (num<0 && num%2==0) {
			System.out.println("El n�mero "+num+" es negativo simplemente");
		} else if (num%2==1 && num>0) {
			System.out.println("El n�mero "+num+" es impar simplemente");
		} else {
			System.out.println("El n�mero "+num+" es impar y negativo al mismo tiempo");
		}{

				}
		} 
		
	}

