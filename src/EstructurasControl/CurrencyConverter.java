package EstructurasControl;

import java.util.Scanner;

/***
 * Ejercicios Estructuras de control 6.3 (p�g. 55)
 * Ejercicio 9
 * Solicitaremos una cantidad monetaria (supuestamente en euros) y un caracter que haga referencia a una divisa (dollar, yen o pound)
 * seg�n la divisa introducida har� la conversion de euros a esa divisa
 * Mostraremos si en base a tu IMC c�mo est�s
 * @author Ferm�n
 *
 */

public class CurrencyConverter {

	public static void main(String[] args) {
		Scanner readfromconsole = new Scanner(System.in);
		Float cantidadEuro;
		String divisaNueva;
		String Line;
		System.out.println("Introduce una cantidad de euros para convertir");
		Line = readfromconsole.nextLine();
		cantidadEuro = Float.parseFloat(Line);
		System.out.println("ahora mete una moneda d Dollar, y yen y p pound");
		divisaNueva = readfromconsole.nextLine();
		
		switch (divisaNueva) {
		case "d":
			System.out.println("La cantidad en d�lares es: "+(cantidadEuro/0.84601));
			break;
		case "y":
			System.out.println("La cantidad en yenes es: "+(cantidadEuro*132.3585));
			break;
		case "p":
			System.out.println("La cantidad en libras es: "+(cantidadEuro*0.8879));
			break;
		default:
		}
		

	}

}
