package EstructurasControl;

import java.util.Scanner;

/***
 * Ejercicios Estructuras de control 6.3 (p�g. 55)
 * Ejercicio 2
 * Vamos a soliciar un n�mero entero y mostraremos por pantalla si
 * el n�mero es positivo, negativo o = 0
 * @author Ferm�n
 *
 */

public class PositiveNegativeOrZero {

	public static void main(String[] args) {
		Scanner readFromConsole = new Scanner(System.in);
		int num;
		String Line="";
		System.out.println("Introduzca un n�mero por favor");
		Line = readFromConsole.nextLine();
		num = Integer.parseInt(Line);
		if (num>0) {
			System.out.println("El n�mero es positivo");
		} else if (num<0) {
			System.out.println("El n�mero es negativo");
		} else {
			System.out.println("el n�mero vale 0");
		}

		
		

	}

}
