package EstructurasControl;

import java.util.Scanner;

/***
 * Ejercicios Estructuras de control 6.3 (p�g. 55)
 * Ejercicio 5
 * Solicitaremos dos n�meros enteros
 * Mostraremos si el primero n�mero es m�ltiplo del segundo
 * @author Ferm�n
 *
 */

public class Multiple {

	public static void main(String[] args) {
		Scanner readFromConsole = new Scanner (System.in);
		int num1, num2;
		String Line = "";
		System.out.println("Introduce un n�mero entero");
		Line = readFromConsole.nextLine();
		num1 = Integer.parseInt(Line);
		System.out.println("Introduce un segundo n�mero, tambi�n entero");
		Line = readFromConsole.nextLine();
		num2 = Integer.parseInt(Line);
		if (num1%num2==0) {
			System.out.println("El primer n�mero es m�ltiplo del segundo");
		} else {
			System.out.println("El primer n�mero no es m�ltiplo del segundo");
		} {

		}

	}

}
