package EstructurasControl;

import java.util.Scanner;

/***
 * Ejercicios Estructuras de control 6.3 (p�g. 55)
 * Ejercicio 6
 * solicitaremos un peso en kg y una altura en cm para calcular el IMC
 * seg�n el resultado mostraremos distintos mensajes
 * @author Ferm�n
 *
 */

public class MassBodyIndexDiagnostic {

	public static void main(String[] args) {
		Scanner readfromconsole = new Scanner(System.in);
		float pesoKilos = 0;
		float alturam = 0;
		String Line;
		float IMC = 0;
		
		System.out.println("Mete peso en Kg:");
		Line = readfromconsole.nextLine();
		pesoKilos = Float.parseFloat (Line);
		System.out.println("Mete altura en metros:");
		Line = readfromconsole.nextLine();
		alturam = Float.parseFloat(Line);
		IMC = (pesoKilos/(alturam*alturam));
		
		if (IMC<16) {
			System.out.println("You need to eat more"); 
		} 
		if (IMC>=16 && IMC<25) {
			System.out.println("You are fine");
		} 
		if (IMC>=25 && IMC<30) {
			System.out.println("You are eating too much");
		}
		if (IMC>=30) {
			System.out.println("Go to hospital");
		}
		

	}



		
	}

