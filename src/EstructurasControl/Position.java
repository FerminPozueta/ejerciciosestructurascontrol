package EstructurasControl;

import java.util.Scanner;

/***
 * Ejercicios Estructuras de control 6.3 (p�g. 55)
 * Ejercicio 8
 * Solicitaremos el dorsal de un jugador
 * comprobaremos que el n�mero est� entre 0 y 9 y nos mostrar� la posici�n del jugador
 * en base al dorsal.
 * Mostraremos si en base a tu IMC c�mo est�s
 * @author Ferm�n
 *
 */

public class Position {

	public static void main(String[] args) {
		Scanner readfromconsole = new Scanner(System.in);
		int dorsal;
		String Line;
		System.out.println("Dinos un n�mero de dorsal entre 0 y 99");
		Line = readfromconsole.nextLine();
		dorsal = Integer.parseInt(Line);
		if (dorsal>= 0 && dorsal <=99) {
			switch (dorsal) {
			case 1:
				System.out.println("Es un Keeper");
				break;
			case 3: case 4: case 5:
				System.out.println("Eres un Defender");
				break;	
			case 6: case 8: case 11:
				System.out.println("Eres un Midfield");
				break;
			case 9:
				System.out.println("Eres un Striker");
				break;	
			default:
				System.out.println("Any");
			}
		} else {
			System.out.println("Te dije que metieras un n�mero del 0 al 99");
		}
	}

}
